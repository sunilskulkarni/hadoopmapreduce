#!/usr/bin/env python3
import sys
from collections import defaultdict


current_word = None
current_count = 0
word = None
word_dic = defaultdict(int)

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, count = line.split('\t', 1)
    #print('%s' % (word))
    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_word == word:
        current_count += count
    else:
        if current_word:
            # write result to STDOUT
            print ('%s\t%s' % (current_word, current_count))
        current_count = count
        current_word = word

    #word_dic[current_word] = current_count

# do not forget to output the last word if needed!
if current_word == word:
    print ('%s\t%s' % (current_word, current_count))
    #word_dic[current_word] = current_count

# sorted_word_dic = sorted(word_dic.items(), key=lambda word_dic: ( word_dic[1], word_dic[0]))[:10]
# #print(sorted_word_dic)
# for release in sorted_word_dic:
#     print('%s\t%s' % (release[0], release[1]))