#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys
from collections import defaultdict

current_word = None
current_count = 0
word = None
word_dic = defaultdict(int)


def top10(current_word, current_count):
    length = len(word_dic)
    #print('Length: %s \t Current Word: %s\tCurrent count: %s' % (len(word_dic), current_word, current_count))
    if (length >= 0 and length < 10):
        word_dic[current_word] = current_count
    elif (length >= 10):
        key_max = max(word_dic, key=word_dic.get)
        key_min = min(word_dic, key=word_dic.get)
        #print('Current Word: %s\tCurrent count: %s\tKey Max:%s\tKey Min:%s' % (current_word, current_count, key_max, key_min))
        if (current_count >= word_dic.get(key_max) or current_count > word_dic.get(key_min)):
            #print('Poped:%s' % (key_min))
            word_dic.pop(key_min)
            word_dic[current_word] = current_count

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, count = line.split('\t', 1)

    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    #print('%s\t%s' % (word, count))
    top10(word, count)


sorted_word_dic = sorted(word_dic.items(), key=lambda word_dic: (-1 * word_dic[1], word_dic[0]))[:10]
reversed_word_dic = sorted(sorted_word_dic, key=lambda sorted_dic: sorted_dic[0])
#print(sorted_word_dic)
for release in reversed_word_dic:
    print ('%s\t%s' % (release[0], release[1]))
