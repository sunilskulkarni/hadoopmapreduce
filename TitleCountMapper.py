#!/usr/bin/env python3

import sys
import re
from typing import List
from collections import defaultdict
from collections import OrderedDict

stopWordsPath = sys.argv[1]
delimitersPath = sys.argv[2]
word_dic = defaultdict(int)
stopwords = []
delimiters = None

with open(stopWordsPath) as f:
   stopwords = f.read().splitlines()

#print((stopwords))
#with open(delimitersPath) as f:
delimiters = "[\\t,;.:?!\\-@\\[\\](){}_*/ ]+"
county = 0

for line in sys.stdin:
    words = [j for j in re.split(delimiters, line.lower().strip()) if j and j not in stopwords]

    for word in words:
        word_dic[word] += 1
        print('%s\t%s' % (word, 1))




# sorted_word_dic = sorted(word_dic.items(), key=lambda word_dic: (-1 * word_dic[1], word_dic[0]))[:10]
# #print(sorted_word_dic)
# for release in sorted_word_dic:
#     print(release[0], release[1])
