#!/usr/bin/env python3

"""reducer.py"""

from operator import itemgetter
import sys
from collections import defaultdict

current_word = None
current_count = 0
rank = 0
word = None
word_dic = defaultdict(int)


# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, count = line.split('\t', 1)
    word_dic[int(word)] = int(count)
    #print('%s\t%s' % (word, count))

s_data = sorted(word_dic.items(), key=lambda item: item[1])
#print(s_data)
rank, count, previous, result = 0, 0, None, {}
for key, num in s_data:

    if(num != previous):
        previous = num
        rank += count
        count = 0

    result[str(key)] = rank
    count += 1


for i in reversed(sorted (result)) :
    #print ((i, result[i]), end ="\n")
    print('%s\t%s' % (i, result[i]))

