# HadoopMapReduce Examples in Python
Reference: https://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/

**1 Overview**
Welcome to the Hadoop MapReduce programming assignment. 

**2 Sorting**
When you are to select top N items in a list, sorting is implicitly needed. Use the following steps to sort:

1. Sort the list ASCENDING based on Firstly count then Secondly on the value. If the value is string, sort lexicographically.

2. Select the bottom N items in the sorted list as Top items.

There is an implementation of this logic in the the third example of the Hadoop MapReduce Tutorial.

For example, to select top 5 items in the list {"A": 100, "B": 99, "C":98, "D": 97, "E": 96, "F": 96, "G":90}, first sort the items ASCENDING:

"G":90

"E": 96

"F": 96

"D": 97

"C":98

"B": 99

"A": 100

Then, the bottom 5 items are A, B, C, D, F.

Another example, to select 5 top items in the list {"43": 100, "12": 99, "44":98, "12": 97, "1": 96, "100": 96, "99":90}

"99":90

"1": 96

"100": 96

"12": 97

"44":98

"12": 99

"43": 100

Then, the bottom 5 items are 43, 12, 44, 12, 100.

**Exercise A: Top Titles**
In this exercise, you are going to implement a counter for words in Wikipedia titles and an application to find the top words used in these titles. To make the implementation easier, we have provided a boilerplate for this exercise in the following files: TitleCountMapper.py, TitleCountReducer.py, TopTitlesMapper.py, TopTitlesReducer.py

Your application takes a huge list of Wikipedia titles (one in each line) as an input and first tokenizes them using provided delimiters, after that make the tokens lowercased, then removes common words from the provided stopwords. Next, your application selects top 10 words, and finally, saves the count for them in the output. Use the method in section 3 Sorting to select top words.

You should first tokenize Wikipedia titles, make the tokens lowercased, remove common words, and save the count for all words in the output with TitleCountMapper.py and TitleCountReducer.py.

You can test your output with:


```
cat /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/titles/titles-a /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/titles/titles-b /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/titles/titles-c /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/titles/titles-d | python3 TitleCountMapper.py stopwords.txt delimiters.txt | sort -k1,1 | python3 TitleCountReducer.py > preA-output_Python

grep "\bcounty\b" preA-output_Python
county  1020

grep "\blist\b" preA-output_Python
list    1948

cat preA-output_Python | python3 TopTitlesMapper.py | sort -k1,1 | python3 TopTitlesReducer.py > A-output_Python
```



**Exercise B: Top Title Statistics**
In this exercise, you are going to implement an application to find some statistics about the top words used in Wikipedia titles. To make the implementation easier, we have provided a boilerplate for this exercise in the following files: TopTitleStatisticsMapper.py, TopTitleStatisticsReducer.py

Your output from Exercise A will be used here. The application saves the following statistics about the top words in the output: “Mean” count, “Sum” of all counts, “Minimum” and “Maximum” of counts, and “Variance” of the counts. All values should be floored to be an integer. For the sake of simplicity, simply use Integer in all calculations.

The following is the sample command we will use to run the application:

```
cat A-output_Python | python3 TopTitleStatisticsMapper.py | sort -k1,1 | python3 TopTitleStatisticsReducer.py > B-output_Python
```

Var is calculated by this formula: Var(X)=E[(X−μ)^2]. Make sure the stats and the corresponding results are tab separated.

**Exercise C: Orphan Pages**
In this exercise, you are going to implement an application to find orphan pages in Wikipedia. 

Your application takes a huge list of Wikipedia links (not Wikipedia titles anymore) as an input. All pages are represented by their ID numbers. Each line starts with a page ID, which is followed by a list of all the pages that the ID has a link to. The following is a sample line in the input:

In this sample, page 2 has links to page 3, 747213, and so on. Note that links are not necessarily two-way. The application should save the IDs of orphan pages in the output. Orphan pages are pages to which no other pages link.

The following is the sample command we will use to run the application:

```
cat /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-a /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-b /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-c /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-d /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-e | python3 OrphanPagesMapper.py | sort -k1,1 | python3 OrphanPagesReducer.py >  C-output_Python
```

**Exercise D: Top Popular Links**
In this exercise, you are going to implement an application to find the most popular pages in Wikipedia. To make the implementation easier, we have provided a boilerplate for this exercise in the following files: LinkCountMapper.py, LinkCountReducer.py, TopPopularLinksMapper.py, TopPopularLinksReducer.py

If you have finished Exercise A, LinkCountMapper.py, LinkCountReducer.py should produce output similar to TitleCountMapper.py, TitleCountReducer.py in Exercise A. Instead of printing the count for each title, LinkCountMapper.py and LinkCountReducer.py should output the link count for each page P, or the number of pages are linked to P. Be careful about the format of output produced by LinkCountMapper.py and LinkCountReducer.py., since the output of them is supposed to be the input of TopPopularLinksMapper.py . So if you use tab to separate the page ID and its link count, your TitleCountMapper.py should also consume its input in this way.

Your application takes a huge list of Wikipedia links as an input. All pages are represented by their ID numbers. Each line starts with a page ID, which is followed by a list of all the pages that the ID has a link to. The following is a sample line in the input:

In this sample, page 2 has links to page 3, 747213, and so on. Note that links are not necessarily two-way. The application should save the IDs of top 10 popular pages as well as the number of links to them in the output. A page is popular if more pages are linked to it. Use the method in section 2 Sorting to select top links.


```
cat /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-a /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-b /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-c /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-d /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/links/links-e | python3 LinkCountMapper.py | sort -k1,1 | python3 LinkCountReducer.py > linkCount-output_Python

cat linkCount-output_Python | python3 TopPopularLinksMapper.py | sort -k1,1 | python3 TopPopularLinksReducer.py > D-output_Python
```




The order of lines matters. Please sort your output (key value) in alphabetic order. Also, make sure the key and value pair in final output are tab separated.

**Exercise E: Popularity League**
In this exercise, you are going to implement an application to find the most popular pages in Wikipedia. To make the implementation easier, we have provided a boilerplate for this exercise in the following file: PopularityLeagueMapper.py, PopularityLeagueReducer.py

Your application takes a huge list of Wikipedia links as an input. All pages are represented by their ID numbers. Each line starts with a page ID, which is followed by a list of all the pages that the ID has a link to. The following is a sample line in the input:

In this sample, page 2 has links to page 3, 747213, and so on. Note that links are not necessarily two-way.

The popularity of a page is determined by the number of pages in the whole Wikipedia graph that link to that specific page. (Same number as Exercise D)

The application also takes a list of page IDs as an input (also called a league list). The goal of the application is to calculate the rank of pages in the league using their popularity.

The rank of the page is the number of pages in the league with strictly less (not equal) popularity than the original page.

The following is the sample command we will use to run the application:

```
cat linkCount-output_Python | python3 PopularityLeagueMapper.py /Users/sunilkulkarni/PycharmProjects/CS498/mp4/MP4_python/dataset/league.txt | sort -k1,1 | python3 PopularityLeagueReducer.py > E-output_Python
```



**Hadoop Commands**
You can run above programs on Hadoop using following commands. Following are examples.

```
$HADOOP_PREFIX/bin/hadoop jar $HADOOP_PREFIX/share/hadoop/tools/lib/hadoop-streaming-2.9.2.jar -files mapper.py,reducer.py -mapper 'python3 mapper.py' -reducer 'python3 reducer.py' -input /gutenberg/* -output /gutenberg/output thon

197  hdfs dfs -ls /gutenberg/output
198  hdfs dfs -cat /gutenberg/output/part-00000


hadoop jar $HADOOP_PREFIX/share/hadoop/tools/lib/hadoop-streaming-2.9.2.jar -files TitleCountMapper.py,TitleCountReducer.py -mapper 'python3 TitleCountMapper.py stopwords.txt delimiters.txt' -reducer 'python3 TitleCountReducer.py' -input dataset/titles/ -output ./preA-output_Python

root@ceda5803e60e:/MP4_python# pwd
/MP4_python
root@ceda5803e60e:/MP4_python# hadoop fs -find preA-output_Pythonthon

root@ceda5803e60e:/MP4_python# hadoop fs -find preA-output_Pythonthon
preA-output_Pythonthon
preA-output_Pythonthon/_SUCCESS
preA-output_Pythonthon/part-00000
root@ceda5803e60e:/MP4_python#
```


