#!/usr/bin/env python3
"""mapper.py"""

import sys
from collections import defaultdict

#word_dic = defaultdict(int)

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into words
    words = line.split()

    # # increase counters
    key = words[0]
    key = key.replace(':','')
    ikey = int(key)
    values = []
    i = 1
    while i < len(words):
        values.append(int(words[i]))
        i += 1

    #word_dic[ikey] = values
    print ('%s\t%s' % (ikey, str(values)[1:-1]))

# list1 = list(word_dic.keys())
# list2 = sum(word_dic.values(), [])
# print(len(list1))
# print(len(list2))
# print ('%s\t%s' % (list(word_dic.keys()), sum(word_dic.values(), [])))
# print ( set(list1) - set(list2))