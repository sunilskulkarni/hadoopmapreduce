#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys
from collections import defaultdict


current_word = None
current_count = 0
word = None
word_dic = defaultdict(int)

# def top10(current_word, current_count):
#     length = len(word_dic)
#     #print('Length: %s \t Current Word: %s\tCurrent count: %s' % (len(word_dic), current_word, current_count))
#     if (length >= 0 and length < 10):
#         word_dic[current_word] = current_count
#     elif (length >= 10):
#         key_max = max(word_dic, key=word_dic.get)
#         key_min = min(word_dic, key=word_dic.get)
#         #print('Current Word: %s\tCurrent count: %s\tKey Max:%s\tKey Min:%s' % (current_word, current_count, key_max, key_min))
#         if (current_count >= word_dic.get(key_max) or current_count > word_dic.get(key_min)):
#             #print('Poped:%s' % (key_min))
#             word_dic.pop(key_min)
#             word_dic[current_word] = current_count

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, count = line.split('\t', 1)


    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    #print('Word: %s\tcount: %s' % (word, count))
    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_word == word:
        current_count += count
    else:
        if current_word:
            print('%s\t%s' % (current_word, current_count))

        current_count = count
        current_word = word


# do not forget to output the last word if needed!
if current_word == word:
    print ('%s\t%s' % (current_word, current_count))
#top10(current_word, current_count)

# if(current_word == '5300058' or current_word == '81615' or  current_word == '1804986' or  current_word == '3294332' or  current_word == '3078798' or  current_word == '1' or  current_word == '3' or current_word == '2370447'):
#                 print('%s\t%s' % (current_word, current_count))

#print('Length: %s\t' % (len(word_dic)))
# sorted_word_dic = sorted(word_dic.items(), key=lambda word_dic: (-1 * word_dic[1], word_dic[0]))[:10]
# #print(sorted_word_dic)
# for release in sorted_word_dic:
#     print ('%s\t%s' % (release[0], release[1]))