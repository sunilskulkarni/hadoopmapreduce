#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys
from collections import defaultdict

word = None
word_dic = defaultdict(int)
sum = 0
count = 0
min = 0
max = 0
sum2 = 0

# input comes from STDIN
for line in sys.stdin:

    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, val = line.split('\t', 1)
    #print ('Line[%s] Input: %s\t%s' % (i, word, count))


    # convert count (currently a string) to int
    try:
        ival = int(val)
        word_dic[word] = ival
        sum += ival
        count += 1
        sum2 = sum2 + ival **2

        if (min == 0):
            min = ival
        else:
            if(min > ival):
                min = ival

        if (max == 0):
            max = ival
        else:
            if(max < ival):
                max = ival

    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

mean = round(sum/count)
variance= round((sum2/count) - (sum/count)**2)
print('Mean\t%s' % mean)
print('Sum\t%s' % sum)
print('Min\t%s' % min)
print('Max\t%s' % max)
print('Var\t%s' % variance)