#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys
from collections import defaultdict

current_word = None
current_count = 0
word = None
word_dic = defaultdict(int)
visited = set()
vertex = set()

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    #print(line)

    # parse the input we got from mapper.py
    key, values = line.split('\t', 1)
    values = values.split(',')

    i = 0
    try:
        vertex.add(int(key))
        while i < len(values):
            #print(int(values[i]))
            visited.add(int(values[i]))
            i += 1

    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

orphan = vertex - visited
print(*sorted(orphan, key=str), sep='\n')