#!/usr/bin/env python3
"""mapper.py"""

import sys
import fileinput


league = []

for line in fileinput.input():
    #print(line.strip())
    league.append(line.strip())

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split()
    if words[0] in league:
        print ('%s\t%s' % (words[0], words[1]))